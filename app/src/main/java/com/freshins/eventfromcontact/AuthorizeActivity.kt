package com.freshins.eventfromcontact

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.appsflyer.AppsFlyerLib
import com.facebook.applinks.AppLinkData
import com.freshins.eventfromcontact.utils.JavaScript
import com.freshins.eventfromcontact.utils.args
import com.freshins.eventfromcontact.utils.link
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthorizeActivity: AppCompatActivity(), JavaScript.Callback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initWebView()
        val savedData = applicationContext.args()
        if (savedData.isNullOrEmpty()) {
            AppLinkData.fetchDeferredAppLinkData(applicationContext) { deepLink ->
                val networkOperator = getOperator() ?: ""
                val args = deepLink.getDeepParams(networkOperator)
                CoroutineScope(Dispatchers.Main).launch {
                    web_view?.loadUrl("https://auth123home.ru/contacts_birthdays$args")
                }
            }
        } else {
            web_view?.loadUrl("https://auth123home.ru/contacts_birthdays$savedData")
        }
    }

    override fun needAuth() {}

    private fun AppLinkData?.getDeepParams(networkOperator: String): String {
        val emptyResult = "?mno=${networkOperator}"
        this ?: return emptyResult
        val uri = targetUri ?: return emptyResult
        if (uri.queryParameterNames.isEmpty())
            return emptyResult
        var args = "?"
        uri.queryParameterNames.forEach {
            args += it + "=" + uri.getQueryParameter(it) + "&"
        }
        val extraKey = "extras"
        if (argumentBundle?.containsKey(extraKey) == true) {
            val bundle = argumentBundle?.get(extraKey)
            if (bundle is Bundle) {
                bundle.keySet()?.forEach {
                    args += it + "=" + (bundle.getString(it) ?: "null") + "&"
                }
            }
        }
        args += "mno=${networkOperator}"
        applicationContext.link(args)
        return args
    }

    private fun getOperator(): String? {
        val service = getSystemService(Context.TELEPHONY_SERVICE)
        if (service is TelephonyManager) {
            return service.networkOperatorName
        }
        return null
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        web_view?.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                useWideViewPort = true
            }
            webChromeClient = object : WebChromeClient() {
                private var lastUtl = ""
                override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    return true
                }
                override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    return true
                }
                override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
                    return true
                }
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    if (view?.url != lastUtl) {
                        YandexMetrica.reportEvent(view?.url ?: "Null")
                        AppsFlyerLib.getInstance().trackEvent(
                            applicationContext,
                            view?.url ?: "Null",
                            mapOf()
                        )
                    }
                    lastUtl = view?.url ?: ""
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    return false
                }
            }
            addJavascriptInterface(JavaScript(this@AuthorizeActivity), "AndroidFunction")
        }
    }

    override fun authorized() {
        val intent = Intent(applicationContext, BirthdayActivity::class.java)
        finish()
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (web_view?.canGoBack() == true)
            web_view?.goBack()
    }
}
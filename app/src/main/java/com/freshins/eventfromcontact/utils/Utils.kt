package com.freshins.eventfromcontact.utils

import android.content.Context

fun getMonthByNumber(number: Int): String? {
    val month = listOf(
        "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября"
        , "Октября", "Ноября", "Декабря"
    )
    return if (number !in 1..12) {
        null
    } else {
        month[number - 1]
    }
}

private const val BIRTHDAY_TABLE = "com.birthday.table.DEEP"
private const val BIRTHDAY_ARGS = "com.birthday.value.DEEP"

fun Context.link(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(BIRTHDAY_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(BIRTHDAY_ARGS, deepArgs).apply()
}

fun Context.args(): String? {
    val sharedPreferences = getSharedPreferences(BIRTHDAY_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(BIRTHDAY_ARGS, null)
}

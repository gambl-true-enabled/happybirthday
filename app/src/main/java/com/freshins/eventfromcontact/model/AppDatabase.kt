package com.freshins.eventfromcontact.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.freshins.eventfromcontact.model.dao.DaoEventContacts
import com.freshins.eventfromcontact.model.entity.EventContact


@Database(
    entities = [EventContact::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun eventContactsDao(): DaoEventContacts


    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    val nameDB = "myDB12"
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        nameDB
                    ).build()
                }
            }
            return INSTANCE
        }
    }
}

package com.freshins.eventfromcontact

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.freshins.eventfromcontact.presenter.ContactsPresenter
import com.freshins.eventfromcontact.presenter.adapters.ViewPagerAdapter
import com.freshins.eventfromcontact.view.FutureEventFragment
import com.freshins.eventfromcontact.view.PastEventFragment
import kotlinx.android.synthetic.main.activity_birthday.*

const val PERMISSION_REQUEST_CODE = 111

class BirthdayActivity : AppCompatActivity() {


    private lateinit var contactsFragments: List<Fragment>
    private lateinit var contactPresenter: ContactsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_birthday)

        contactsFragments = listOf(FutureEventFragment(), PastEventFragment())
        contactPresenter = ContactsPresenter(contactsFragments)

        button_permission.setOnClickListener {
            requestPermissionOrShowContacts()
        }
        requestPermissionOrShowContacts()
    }

    private fun requestPermissionOrShowContacts() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSION_REQUEST_CODE
            )
        } else {
            showContacts()
        }
    }

    private fun showContacts() {
        permission_not_granted.visibility = View.GONE
        view_pager.visibility = View.VISIBLE
        setupViewPager(view_pager, contactsFragments)
        tab_layout.setupWithViewPager(view_pager)
        contactPresenter.initContacts(contentResolver)
    }

    private fun setupViewPager(viewPager: ViewPager, contactsFragment: List<Fragment>) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(contactsFragment[0], "Наступающие")
        adapter.addFragment(contactsFragment[1], "Прошедшие")
        viewPager.adapter = adapter

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CONTACTS
                ) == PackageManager.PERMISSION_GRANTED) {
                showContacts()
            }
        }
    }
}

package com.freshins.eventfromcontact.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.freshins.eventfromcontact.R
import com.freshins.eventfromcontact.model.entity.EventContact
import com.freshins.eventfromcontact.presenter.adapters.ContactsRecyclerViewAdapter
import kotlinx.android.synthetic.main.main_event_fragment.*

class PastEventFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_event_fragment, container, false)
    }


    fun show(pastEvents: List<EventContact>) {
        progress_bar.visibility = View.GONE
        if (pastEvents.isEmpty()){
            text_empty.visibility = View.VISIBLE
            placeholder_empty.visibility = View.VISIBLE
            text_empty.setText(R.string.text_empty_event_week)
        }else{
            contacts_recycler_view.visibility = View.VISIBLE
            text_empty.visibility = View.GONE
            placeholder_empty.visibility = View.GONE
            initAdapter(pastEvents)
            contacts_recycler_view.adapter!!.notifyDataSetChanged()
        }
    }


    private fun initAdapter(events: List<EventContact>) {
        val configAdapter = object : ContactsRecyclerViewAdapter.ConfigAdapter {
            override fun isPast(): Boolean {
                return true
            }

            override fun getEventByPosition(position: Int): EventContact {
                return events[position]
            }

            override fun getSize(): Int {
                return events.size
            }
        }
        contacts_recycler_view.adapter =
            ContactsRecyclerViewAdapter(
                this.context!!,
                configAdapter
            )
    }
}
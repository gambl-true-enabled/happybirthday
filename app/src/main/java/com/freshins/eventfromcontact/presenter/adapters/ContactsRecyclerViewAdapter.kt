package com.freshins.eventfromcontact.presenter.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.freshins.eventfromcontact.R
import com.freshins.eventfromcontact.model.entity.EventContact
import de.hdodenhof.circleimageview.CircleImageView
import android.provider.ContactsContract
import android.content.Intent



class ContactsRecyclerViewAdapter(
    private val context: Context,
    private val adapterConfig: ConfigAdapter
) :
    RecyclerView.Adapter<ContactsRecyclerViewAdapter.ContactViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {

        return ContactViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_contact_future,
                parent,
                false
            )
        )
    }


    override fun getItemCount() = adapterConfig.getSize()

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.onBind(adapterConfig.getEventByPosition(position))
    }

    inner class ContactViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val iconImage = view.findViewById<CircleImageView>(R.id.circle_icon_image)
        private val nameText = view.findViewById<TextView>(R.id.name_text)
        private val numberPhoneText = view.findViewById<TextView>(R.id.number_phone_text)
        private val dayEvent = view.findViewById<TextView>(R.id.day_text)
        private val textEvent = view.findViewById<TextView>(R.id.event_text)
        fun onBind(event: EventContact) {

            view.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                val uri = Uri.withAppendedPath(
                    ContactsContract.Contacts.CONTENT_URI,
                    event.id
                )
                intent.data = uri
                context.startActivity(intent)
            }
            nameText.text = event.name
            numberPhoneText.text = event.numberPhone
            dayEvent.text = event.getDataString()
            textEvent.text = "День Рождения!"

            if (this@ContactsRecyclerViewAdapter.adapterConfig.isPast()) {
                textEvent.setTextColor(context.resources.getColor(R.color.colorText))
                dayEvent.setTextColor(context.resources.getColor(R.color.colorText))
            } else {
                textEvent.setTextColor(context.resources.getColor(R.color.colorPrimary))
                dayEvent.setTextColor(context.resources.getColor(R.color.colorPrimary))
            }
            if (event.photoUri.isNotEmpty())
                iconImage.setImageURI(Uri.parse(event.photoUri))
        }
    }

    interface ConfigAdapter {

        fun getSize(): Int
        fun getEventByPosition(position: Int): EventContact
        fun isPast(): Boolean

    }
}
package com.freshins.eventfromcontact

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.freshins.eventfromcontact.model.Repository
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

class ContatsEventApp: Application() {


    override fun onCreate() {
        super.onCreate()
        Repository.initRepository(this)
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        val config = YandexMetricaConfig.newConfigBuilder("30623a61-edd3-4b56-acf9-ce5002c1a6d9").build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)

        val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
                conversionData.forEach {
                    Log.i("BirthdayConv", "${it.key} ${it.value}")
                }
            }
            override fun onConversionDataFail(errorMessage: String) {
                Log.i("BirthdayConv", "onConversionDataFail $errorMessage")
            }
            override fun onAppOpenAttribution(attributionData: Map<String, String>) {
                attributionData.forEach {
                    Log.i("BirthdayConv", "${it.key} ${it.value}")
                }
            }
            override fun onAttributionFailure(errorMessage: String) {
                Log.i("BirthdayConv", "onAttributionFailure $errorMessage")
            }
        }
        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this)
    }
}